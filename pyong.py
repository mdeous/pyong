#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lib.game import GameManager


def main():
    game_manager = GameManager()
    game_manager.run()


if __name__ == '__main__':
    main()
