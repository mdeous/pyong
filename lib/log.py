# -*- coding: utf-8 -*-

import logging


class ColorFormatter(logging.Formatter):
    """
    A logging formatter that displays the loglevel with colors and
    the logger name in bold.
    """
    _colors_map = {
        'DEBUG': '\033[22;32m',
        'INFO': '\033[01;34m',
        'WARNING': '\033[22;35m',
        'ERROR': '\033[22;31m',
        'CRITICAL': '\033[01;31m'
    }

    def format(self, record):
        """
        Overrides the default Formatter.format method to add colors to
        the levelname and name attributes.
        """
        if record.levelname in self._colors_map:
            record.msg = '{0}{1}\033[0;0m'.format(
                self._colors_map[record.levelname],
                record.msg
            )
            record.levelname = '{0}{1}\033[0;0m'.format(
                self._colors_map[record.levelname],
                record.levelname
            )
        record.name = '\033[37m\033[1m{0}\033[0;0m'.format(record.name)
        return super(ColorFormatter, self).format(record)


def create_logger(name, level='INFO'):
    """
    Creates a new logger object.
    """
    formatter = ColorFormatter(
        '[ %(asctime)s ][ %(levelname)s ][ %(name)s ] %(message)s',
        '%Y-%m-%d %H:%M:%S'
    )
    if not isinstance(logging.getLevelName(level), int):
        level = 'INFO'
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    return logger
