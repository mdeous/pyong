# -*- coding: utf-8 -*-

import math
import random

import pygame

from lib.globals import *
from lib.helpers import load_image
from lib.log import create_logger


class BaseSprite(pygame.sprite.Sprite):
    """
    Base of all Pyong sprites.
    """
    image_filename = None
    start_position = None

    def __init__(self, manager):
        super(BaseSprite, self).__init__()
        self.log = create_logger('%s-%s' % (self.__class__.__name__, id(self)), LOG_LEVEL)
        self.manager = manager
        screen = pygame.display.get_surface()
        self.area = screen.get_rect()
        if self.image_filename is not None:
            self.image, self.rect = self.load_image(self.image_filename)
            self.reset_position()

    @property
    def x_min(self):
        """
        Minimum X value where sprite doesn't go past the left screen border.
        """
        return self.area.left + self.rect.width / 2

    @property
    def x_max(self):
        """
        Maximum X value where sprite doesn't go past the right screen border.
        """
        return self.area.right - self.rect.width / 2

    @property
    def y_min(self):
        """
        Minimum Y value where sprite doesn't go past the upper screen border.
        """
        return self.area.top - self.rect.height / 2

    @property
    def y_max(self):
        """
        Maximum Y value where sprite doesn't go past the lower screen border.
        """
        return self.area.bottom - self.rect.height / 2

    def load_image(self, image_file):
        """
        Loads/replaces the image used for the sprite.
        """
        self.log.debug('loading image file : %s' % image_file)
        return load_image(image_file)

    def set_position(self, xy):
        """
        Moves sprite to given xy coordinates.
        """
        self.rect.midtop = xy

    def reset_position(self):
        """
        Resets sprite to its initial position.
        """
        if self.start_position is not None:
            self.set_position(self.start_position)

    def collides(self, rect):
        """
        Checks if sprite edges collide with given Rect.
        """
        top = rect.collidepoint(self.rect.midtop)
        bottom = rect.collidepoint(self.rect.midbottom)
        left = rect.collidepoint(self.rect.midleft)
        right = rect.collidepoint(self.rect.midright)
        return top, bottom, left, right

    def update(self):
        """
        Should be implemented by all sprites to define their position at each tick.
        """
        raise NotImplementedError


class BaseTextSprite(BaseSprite):
    """
    Base sprite implementing methods to display text.
    """
    def __init__(self, manager):
        super(BaseTextSprite, self).__init__(manager)
        self.value = ''
        self.font = pygame.font.SysFont('Arial Sans', 28)

    def update(self):
        new_value = self.get_value()
        if new_value != self.value:
            self.value = new_value
            self.image = self.font.render(new_value, 1, (0, 0, 0))
            self.rect = self.image.get_rect()
            self.rect.bottomright = self.get_position()
            self.manager.screen.blit(
                    self.image,
                    self
            )

    def get_position(self):
        """
        Should return the position where the sprite's bottomleft should be positioned.
        """
        raise NotImplementedError

    def get_value(self):
        """
        Should return the text to be displayed.
        """
        raise NotImplementedError


class PlayerBar(BaseSprite):
    """
    Player controlled bar.
    """
    image_filename = 'player_bar.png'
    start_position = (400, 500)

    def __init__(self, manager):
        super(PlayerBar, self).__init__(manager)
        self.fire_pressed = False
        self.ball_fired = False

    def update(self):
        # make the bar follow the mouse's Y position without going past the screen's borders
        x_coord = pygame.mouse.get_pos()[0]
        if (x_coord != self.rect.midtop[0]) and (x_coord >= self.x_min) and (x_coord <= self.x_max):
            self.rect.midtop = (x_coord, 500)

    def start_fire(self):
        """
        Fire button has been pressed.
        """
        if not self.fire_pressed and not self.ball_fired:
            self.log.debug('firing ball')
            self.fire_pressed = True
            self.ball_fired = True
            pass

    def stop_fire(self):
        """
        Fire button has been released.
        """
        self.fire_pressed = False


class Ball(BaseSprite):
    """
    Ball launched by PlayerBar to break Brick's.
    """
    image_filename = 'ball.png'
    start_position = (400, 499)
    start_speed = 5

    def __init__(self, manager):
        super(Ball, self).__init__(manager)
        self.speed = self.start_speed
        self.direction = self.random_direction()

    @staticmethod
    def random_direction():
        """
        Computes a random ball throw direction.
        """
        return random.randint(-2000, -200) / 1000

    def move(self):
        """
        Moves the sprite according to its current vector.
        """
        dx = self.speed * math.cos(self.direction)
        dy = self.speed * math.sin(self.direction)
        self.rect = self.rect.move(dx, dy)

    def update(self):
        player_bar = self.manager.player_bar

        # ball is still attached to the player bar, move it accordingly
        if (not player_bar.ball_fired) and (self.rect.midbottom != player_bar.rect.midtop):
            player_bar_x, player_bar_y = player_bar.rect.midtop
            self.rect.midtop = (player_bar_x, player_bar_y - self.rect.height + 1)

        # ball is moving, compute next position while avoiding collisions
        elif player_bar.ball_fired:
            self.move()

            # check if ball goes out of screen
            if not self.area.contains(self.rect):
                top, bottom, left, right = map(lambda i: not i, self.collides(self.area))
                if top:
                    self.direction = -self.direction
                elif bottom:
                    self.manager.lost_ball()
                elif left or right:
                    self.direction = math.pi - self.direction

            # check if ball hits the player's bar
            elif player_bar.rect.contains(self.rect):
                top, bottom, _, _ = self.collides(player_bar.rect)
                if top or bottom:
                    self.direction = -self.direction
                self.log.debug('PlayerBar-%s hit' % id(player_bar))

            # check if ball hits a brick
            else:
                collide_brick = self.rect.collidelist(self.manager.bricks)
                if collide_brick != -1:
                    brick = self.manager.bricks[collide_brick]
                    self.log.debug('Brick-%s hit' % id(brick))
                    self.manager.level.break_brick()
                    top, bottom, left, right = self.collides(brick.rect)
                    if top or bottom:
                        self.direction = - self.direction
                    elif left or right:
                        self.direction = math.pi - self.direction
                    brick_killed = brick.hit()
                    if brick_killed:
                        del self.manager.bricks[collide_brick]
        else:
            return


class Brick(BaseSprite):
    """
    Brick to be broken using the Ball.
    """
    image_filename = 'brick.png'

    def __init__(self, manager, strength):
        super(Brick, self).__init__(manager)
        self.strength = strength

    def hit(self):
        self.strength -= 1
        if self.strength == 0:
            self.kill()
            return True
        return False

    def update(self):
        pass


class ScoreText(BaseTextSprite):
    """
    Text sprite displaying the user's score.
    """
    def get_position(self):
        return self.manager.screen.get_width(), self.manager.screen.get_height()

    def get_value(self):
        score = (self.manager.level.start_bricks - self.manager.level.bricks_count) * 10
        return 'Score: %d ' % score


class LevelText(BaseTextSprite):
    """
    Text sprite displaying current level.
    """
    def get_position(self):
        return self.image.get_width(), self.manager.screen.get_height()

    def get_value(self):
        return ' Level %d' % self.manager.level.number
