# -*- coding: utf-8 -*-

import os

import pygame
from pygame.locals import *

from lib.globals import LOG_LEVEL
from lib.helpers import level_exists
from lib.log import create_logger
from lib.sprites import PlayerBar, Ball, Brick, ScoreText, LevelText


class GameManager(object):
    def __init__(self):
        self.log = create_logger(self.__class__.__name__, LOG_LEVEL)
        self.screen = None
        self.background = None
        self.clock = None
        self.player_bar = None
        self.ball = None
        self.bricks = []
        self.status_text = None
        self.level_text = None
        self.level = None
        self.all_sprites = pygame.sprite.Group()

    def lost_ball(self):
        """
        Called when the ball passes screen's bottom edge.
        """
        self.log.info('ball lost')
        # Reset sprites position
        for sprite in self.all_sprites:
            sprite.reset_position()
        # Reset bar events
        self.player_bar.fire_pressed = False
        self.player_bar.ball_fired = False
        # Reset ball direction and speed
        self.ball.direction = self.ball.random_direction()
        self.ball.speed = self.ball.start_speed

    def load_level(self, number):
        if self.player_bar is None:
            self.player_bar = PlayerBar(self)
            self.all_sprites.add(self.player_bar)
        else:
            self.player_bar.ball_fired = False
            self.player_bar.fire_pressed = False
        if self.ball is None:
            self.ball = Ball(self)
            self.all_sprites.add(self.ball)
        if self.status_text is None:
            self.status_text = ScoreText(self)
            self.all_sprites.add(self.status_text)
        if self.level_text is None:
            self.level_text = LevelText(self)
            self.all_sprites.add(self.level_text)
        self.level = Level(self, number)
        self.bricks = self.level.read_map_file()
        self.all_sprites.add(*self.bricks)

    def run(self):
        """
        Initializes the game and runs the main loop.
        """
        self.log.info('initializing pygame')
        pygame.init()
        self.screen = pygame.display.set_mode((800, 600))
        pygame.display.set_caption('Pyong')
        pygame.mouse.set_visible(0)

        self.log.info('setting up background')
        self.background = pygame.Surface(self.screen.get_size()).convert()
        self.background.fill((255, 255, 255))
        self.screen.blit(self.background, (0, 0))
        pygame.display.flip()

        self.log.info('creating game objects')
        self.clock = pygame.time.Clock()
        self.load_level(1)

        # Main loop
        self.log.info('starting main loop')
        while True:
            self.clock.tick(60)
            for event in pygame.event.get():
                if (event.type == QUIT) or (event.type == KEYDOWN and event.key == K_ESCAPE):
                    self.log.info('exiting game')
                    return
                elif (event.type == MOUSEBUTTONDOWN) or (event.type == KEYDOWN and event.key == K_SPACE):
                    self.player_bar.start_fire()
                elif (event.type == MOUSEBUTTONUP) or (event.type == KEYUP and event.key == K_SPACE):
                    self.player_bar.stop_fire()

            self.all_sprites.update()
            self.screen.blit(self.background, (0, 0))
            self.all_sprites.draw(self.screen)
            pygame.display.flip()
            if self.level.won:
                self.log.info('level %d won' % self.level.number)
                next_level = self.level.number + 1
                if level_exists(next_level):
                    self.load_level(next_level)
                else:
                    self.log.info('GAME FINISHED, CONGRATULATIONS!')
                    return


class Level(object):
    """
    A representation of a game level.
    """
    top_padding = 100

    def __init__(self, manager, number):
        self.log = create_logger('%s-%d' % (self.__class__.__name__, number), LOG_LEVEL)
        self.manager = manager
        self.number = number
        self.map_filename = os.path.join('data', 'levels', 'level-%d' % number)
        self.start_bricks = None
        self.bricks_count = None

    @property
    def won(self):
        """
        Game is won if there are no more bricks left.
        """
        if self.bricks_count is not None and self.bricks_count == 0:
            self.log.info('level finished')
            return True
        return False

    def read_map_file(self):
        """
        Reads the level's map file and returns initialized Brick objects;
        """
        self.bricks_count = 0
        bricks = []
        with open(self.map_filename) as ifile:
            for line_num, line in enumerate(ifile):
                line = line.strip('\r\n')
                if len(line) != 25:
                    self.log.warning('wrong length (%d) for line %d' % (len(line), line_num+1))
                    continue
                for brick_num, brick_strength in enumerate(line):
                    brick_strength = brick_strength.strip()
                    if brick_strength:
                        # create brick and compute its position
                        brick = Brick(self.manager, int(brick_strength))
                        x_pos = (brick.rect.width / 2) + (brick.rect.width * brick_num)
                        y_pos = brick.rect.height * line_num + self.top_padding
                        brick.set_position((x_pos, y_pos))
                        bricks.append(brick)
                        self.bricks_count += 1
        self.start_bricks = self.bricks_count
        return bricks

    def break_brick(self):
        """
        Called when a brick is hit
        """
        self.bricks_count -= 1
        self.log.info('brick broken, %d bricks remaining' % self.bricks_count)
