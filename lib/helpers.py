# -*- coding: utf-8 -*-

import os

import pygame
from pygame.locals import *

from lib.globals import LOG_LEVEL
from lib.log import create_logger

log = create_logger(__name__, LOG_LEVEL)


class NoneSound(object):
    """
    Dummy sound class used when pygame.mixer isn't available
    """

    def play(self):
        pass


def load_image(filename):
    """
    Loads an image file and returns a (Image, Rect) tuple.
    """
    fullname = os.path.join('data', 'graphics', filename)
    try:
        image = pygame.image.load(fullname)
    except pygame.error, message:
        log.critical('can not load image : %s' % filename)
        raise SystemExit, message
    image = image.convert()
    colorkey = image.get_at((0, 0))
    image.set_colorkey(colorkey, RLEACCEL)
    return image, image.get_rect()


def load_sound(filename):
    """
    Loads a sound file and returns a Sound object.
    """
    if not pygame.mixer:
        log.warning('pygame.mixer not available, can not load sound file : %s' % filename)
        return NoneSound()
    fullname = os.path.join('data', 'sounds', filename)
    try:
        sound = pygame.mixer.Sound(fullname)
    except pygame.error, message:
        log.critical('can not load sound : %s' % filename)
        raise SystemExit, message
    return sound


def level_exists(number):
    level_filename = os.path.join('data', 'levels', 'level-%d' % number)
    return os.path.exists(level_filename)
